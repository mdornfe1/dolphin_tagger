from IPython import embed
import tensorflow as tf
import numpy as np
from numpy.random import permutation
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D
from keras.regularizers import l2
from keras.metrics import mean_squared_error
from keras.objectives import binary_crossentropy

from image_stream import ImageStream
from utils import reduce_median, list_to_queue

def custom_loss(x, x_hat):
	"""
	Custom loss function for training background extraction networks (autoencoders).
	"""

	#flatten x, x_hat before computing mean, median
	shape = x_hat.get_shape().as_list()
	batch_size = shape[0]
	image_size = np.prod(shape[1:])

	x = tf.reshape(x, [batch_size, image_size])
	x_hat = tf.reshape(x_hat, [batch_size, image_size]) 
	
	B0 = reduce_median(tf.transpose(x_hat))
	# I divide by sigma in the next step. So I add a small float32 to F0
	# so as to prevent sigma from becoming 0 or Nan.
	
	F0 = tf.abs(x_hat - B0) + 1e-10
	
	sigma = tf.reduce_mean(tf.sqrt(F0 / 0.5), axis=0)
	
	background_term = tf.reduce_mean(F0 / sigma, axis=-1)
	
	bce = binary_crossentropy(x, x_hat)
	
	loss = bce + background_term 

	return loss

def create_train_generator(frame_numbers, batch_size, image_folder):
		"""
		frame_numbers : Queue
			Queue of frame numbers to be decompressed from ImageStream vs.
			Default is to decompress all frames of vs in order.

		batch_size : int
			Training batch_size.

		image_folder : str
			Location of image files.

		Yields
			(batch, batch) : Tuple[np.ndarray, np.ndarray]
				A tuple of identical batches of images to be used for 
				training a Keras autoencoder.

		Returns a generator that can be used to train a TensorFlow network with
		images from a folder.
		"""

		ims = ImageStream(folder_name=image_folder, color=False)

		while True:
			batch = []
			for _ in range(batch_size):
				i = frame_numbers.get()
				#frame = vs[i].flatten()
				frame = ims[i]
				frame = np.expand_dims(frame, axis=-1)
				batch.append(frame)

			batch = np.array(batch)

			yield (batch, batch)


def build_conv_autoencoder(lambda_W, input_shape, num_filters, optimizer, loss):
	layers = []
	nb_row = 3
	nb_col = 3
	num_pixels = np.product(input_shape)
	with tf.device('/gpu:1'):

		#encoding layers
		
		layers.append(
			Conv2D(input_shape=input_shape, nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[0], border_mode='same', activation='relu', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

		layers.append(
			MaxPooling2D(pool_size=(2,2), border_mode='same') )

		layers.append(
			Conv2D(nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[1], border_mode='same', activation='relu', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

		layers.append(
			MaxPooling2D(pool_size=(2,2), border_mode='same') )

		#decoding layers

		layers.append(
			Conv2D(nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[2], border_mode='same', activation='relu', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

		layers.append(
			UpSampling2D(size=(2,2)) )

		
		layers.append(
			Conv2D(nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[3], border_mode='same', activation='relu', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

		layers.append(
			UpSampling2D(size=(2,2)) )
		

		layers.append(
			Conv2D(nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[4], border_mode='same', activation='sigmoid', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

	autoencoder = Sequential()
	for layer in layers:
		autoencoder.add(layer)

	autoencoder.compile(optimizer, loss, metrics=[mean_squared_error])

	return autoencoder

#hyper parameters
batch_size = 5
nb_epoch = 100
nb_row = 3
nb_col = 3
num_filters = [3, 3, 3, 3, 1]
lambda_W = 0.001
optimizer = 'adam'


#data parameters
image_folder = './test_data/jpegs/boats'
ims = ImageStream(image_folder, color=False)
num_frames = len(ims)
frame_numbers = permutation( np.arange(num_frames-1) )
train_frame_numbers = [int(i) for i in frame_numbers[:25]]
val_frame_numbers = [int(i) for i in frame_numbers[25:]]
num_train_frames = len(train_frame_numbers)
num_val_frames = len(val_frame_numbers)
input_shape = (*ims.shape, 1)

#build and train autoencoder
autoencoder = build_conv_autoencoder(lambda_W, input_shape, num_filters, optimizer, custom_loss)

train_frame_numbers_queue = list_to_queue(train_frame_numbers, n_copies=2000)
train_gen = create_train_generator(train_frame_numbers_queue, batch_size, image_folder)

autoencoder.fit_generator(generator=train_gen, nb_epoch=nb_epoch, 
	samples_per_epoch=num_train_frames)
