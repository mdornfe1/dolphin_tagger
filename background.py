import threading
from typing import List, Generator, Tuple
from queue import Queue, deque
from tempfile import NamedTemporaryFile, mkdtemp
import os
from collections import namedtuple

import tensorflow as tf
import keras
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout
from keras.regularizers import l2
from keras.engine.topology import Layer, InputSpec
from keras import backend as K
import numpy as np
import matplotlib.pyplot as plt
from IPython import embed

from global_vars import QUADRO, TESLA, VIDEO_FOLDER
from video_stream import (VideoStream, FullyConnectedVideoStream, 
	get_files_in_folder)

MAX_Q_SIZE = 10
LAMBDA_SIGMAS = np.arange(0.1, 1.01, 0.2)
video_files = get_files_in_folder(VIDEO_FOLDER)


class LossHistory(keras.callbacks.Callback):
	def on_train_begin(self, logs={}):
		self.losses = []

	def on_batch_end(self, batch, logs={}):
		self.losses.append(float(logs.get('loss')))


def train_generator(
	vs : VideoStream,
	frame_numbers : Queue, 
	batch_size : int,
	predict : bool = False) -> Generator[Tuple[np.ndarray, np.ndarray], None, None]:
		"""
		vs : VideoStream
			An object that makes a connection to the video file which will
			decompressed and used in neural network training.
		frame_numbers : List[int]
			List of frame numbers to be decompressed from VideoStream vs.
			Default is to decompress all frames of vs in order.

		Yields
			(batch, batch) : tuple
				A tuple of identical batches of images to be used for 
				training a Keras autoencoder.

		Returns a generator that can be used to train a TensorFlow network with
		images from a video file.
		"""

		vs = FullyConnectedVideoStream(
			vs.file_name, vs.color, vs.chunk_idx, vs.total_chunks)

		while True:
			batch = []
			for _ in range(batch_size):
				i = frame_numbers.get()
				batch.append(vs[i])

			batch = np.vstack(batch)

			#print('frame_numbers_size = {}'.format(frame_numbers.qsize()))
			if predict:
				print('Training examples left to predict = {}'.format(frame_numbers.qsize() - MAX_Q_SIZE), end='\r')
				yield batch
			else:
				yield (batch, batch)


def build_ben(lambda_W, dropout_rate, num_neurons, optimizer, loss, **kwargs):
	layers = []
	with tf.device(TESLA):
		layers.append(
			Dense(input_dim=num_neurons[0], output_dim=num_neurons[1], activation='relu', W_regularizer=l2(lambda_W), init='glorot_uniform'))
		layers.append(Dropout(dropout_rate))
		layers.append(
			Dense(input_dim=num_neurons[1], output_dim=num_neurons[2], activation='relu', W_regularizer=l2(lambda_W), init='glorot_uniform'))
		layers.append(Dropout(dropout_rate))
	
	with tf.device(QUADRO):
		layers.append(Dense(input_dim=num_neurons[2], output_dim=num_neurons[3], activation='relu', W_regularizer=l2(lambda_W), init='glorot_uniform'))
		layers.append(Dropout(dropout_rate))
		layers.append(Dense(input_dim=num_neurons[3], output_dim=num_neurons[4], activation='sigmoid', W_regularizer=l2(lambda_W), init='glorot_uniform'))

	ben = Sequential()
	for layer in layers:
		ben.add(layer)

	ben.compile(optimizer, loss)

	return ben


def train_ben(ben, frame_numbers, video_file_name, chunk_idx, total_chunks, batch_size, nb_epoch, **kwargs):

	frame_numbers_queue = Queue()
	frame_numbers_queue.queue = deque(frame_numbers)
	vs = FullyConnectedVideoStream(video_file_name, 
	chunk_idx=chunk_idx, total_chunks=total_chunks)
	train_gen = train_generator(vs, frame_numbers_queue, batch_size)
	history = LossHistory()
	nb_worker = 10
	num_samples = len(frame_numbers) / batch_size - MAX_Q_SIZE
	print('\n training ben {}/{}'.format(chunk_idx+1, total_chunks), end='\n\n')
	ben.fit_generator(train_gen, samples_per_epoch=num_samples, 
		nb_epoch=nb_epoch, callbacks=[history], nb_worker=nb_worker, 
		max_q_size=MAX_Q_SIZE)

	return ben, history.losses


def predict_on_training_set(ben, video_file_name, chunk_idx, total_chunks, frame_numbers, **kwargs):
	frame_numbers_queue = Queue()
	frame_numbers_queue.queue = deque(frame_numbers)
	vs = FullyConnectedVideoStream(video_file_name, 
		chunk_idx=chunk_idx, total_chunks=total_chunks)
	gen = train_generator(vs, frame_numbers_queue, batch_size=1, predict=True)

	nb_worker = 10
	num_samples = len(frame_numbers) - MAX_Q_SIZE
	x_hat = ben.predict_generator(gen, val_samples=num_samples, nb_worker=nb_worker, 
		max_q_size=MAX_Q_SIZE)

	return x_hat


def calc_static_background(x_hat):

	B0 = np.median(x_hat, axis=0)
	B0.shape = (1, len(B0))

	return B0


def calc_dynamic_backgrounds(x_hat, B0, lambda_sigma):
	sigma = np.mean(np.abs((x_hat - B0) / lambda_sigma), axis=0)
	mask =  np.abs((x_hat-B0)) <= sigma
	true_idxs = np.where(mask==True)
	B = np.repeat(B0, repeats=len(x_hat), axis=0)
	B[true_idxs] = x_hat[true_idxs]

	return B


def train_bln(bln, B, nb_epoch, batch_size, chunk_idx, total_chunks, **kwargs):

	print('\nTraining bln {}\{} for lambda_sigma={}'.format(chunk_idx+1, total_chunks, lambda_sigma), end='\n\n')
	history = LossHistory()
	
	bln.fit(B, B, nb_epoch=nb_epoch, batch_size=batch_size, callbacks=[history])

	return bln, history.losses


def reshape_plot(x, vs):
	x.shape = vs.chunk_size
	fig, ax = plt.subplots()
	ax.imshow(x, cmap='gray')


def predict_and_plot(ben, vs, frame_idx):
	frame = vs[frame_idx]
	frame_predicted = ben.predict(frame)
	frame.shape = vs.chunk_size
	frame_predicted.shape = vs.chunk_size
	fig, ax = plt.subplots(1, 2)
	ax[0].imshow(frame, cmap='gray')
	ax[1].imshow(frame_predicted, cmap='gray')

	return fig, ax

def build_params_dict(video_file_name, chunk_idxs, total_chunks, frame_numbers, lambda_W, dropout_rate):  
	
	vs = FullyConnectedVideoStream(video_file_name, 
		chunk_idx=chunk_idx, total_chunks=total_chunks)

	layer0_size = vs.chunk_size[0] * vs.chunk_size[1]
	layer1_size = layer0_size // 2
	layer2_size = layer1_size // 2
	num_neurons = (layer0_size, layer1_size, layer2_size, layer1_size, layer0_size)

	batch_size = 1
	nb_epoch = 1
	
	ben_param = dict(lambda_W=lambda_W, dropout_rate=dropout_rate, 
		num_neurons=num_neurons, optimizer='adam', loss='binary_crossentropy',
		batch_size=batch_size, nb_epoch=nb_epoch, video_file_name=video_file_name, frame_numbers=frame_numbers, 
		chunk_idx=chunk_idx, total_chunks = total_chunks)

	return ben_param

video_file_name = video_files[0]
frame_numbers = range(50)
total_chunks = 20
lambda_W = 0.001
dropout_rate = 0.2
for chunk_idx in range(total_chunks):
	ben_param = build_params_dict(video_file_name, chunk_idx, total_chunks, frame_numbers, lambda_W, dropout_rate)
	ben = build_ben(**ben_param)
	ben, loss_history = train_ben(ben, **ben_param)
	ben_param['loss'] = loss_history
	x_hat = predict_on_training_set(ben, **ben_param)

	weights_dir = mkdtemp(dir='./saved_weights', prefix='weights_')
	ben_checkpoint = NamedTemporaryFile(
		dir=weights_dir, prefix='ben_', suffix='.h5', delete=False)

	ben.save(ben_checkpoint.name)
	ben_param['ben_weights_file_name'] = ben_checkpoint.name
	B0 = calc_static_background(x_hat)
	ben_param['B0'] = B0

	bln = ben
	bln_params = []
	for lambda_sigma in LAMBDA_SIGMAS:
		B = calc_dynamic_backgrounds(x_hat, B0, lambda_sigma)
		
		bln_param = ben_param.copy()
		bln_param['lambda_sigma'] = lambda_sigma
		bln_param['B0'] = B0
		bln_param['B'] = B

		bln, loss_history = train_bln(bln, **bln_param)

		bln_param['loss_history'] = loss_history

		bln_checkpoint = NamedTemporaryFile(
			dir=weights_dir, prefix='bln_', suffix='.h5', delete=False)

		bln.save(bln_checkpoint.name)
		
		bln_param['bln_weights_file_name'] = bln_checkpoint.name
		
		bln_params.append(bln_param)

		#End the current and start a new tensorflow session to free up gpu memory 
		#to allow the next bln to be trained.
		K.get_session().close()
		sess = tf.Session()
		K.set_session(sess)

		bln = load_model(ben_checkpoint.name)

embed()
