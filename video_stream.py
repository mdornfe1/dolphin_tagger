#!/usr/bin/python3

"""
Class for Pythonic indexing and interation of a video file.

Author: Matthew Dornfeld
email: mdornfe1@gmail.com
"""

from moviepy.editor import VideoFileClip
from skimage import color
import numpy as np
import matplotlib.pyplot as plt
import os, sys, contextlib
from queue import Queue
from typing import List
from math import floor

plt.style.use('bmh')

def get_files_in_folder(folder:str) -> List[str]:
	"""
	folder : str
		folder path
	
	Returns
		full_file_paths : List[str]
			A list of full file paths of the files in the folder.

	"""
	files = os.listdir(folder)
	full_file_paths = []
	for file in files:
		full_file_path = os.path.join(folder, file)
		full_file_paths.append(full_file_path)

	return full_file_paths


class VideoStream:
	"""
	Interface to provide easy iteration and indexing access to moviepy
	video objects. 

	Instructions: 
	vs = VideoStream('/path/to/file.mp4')

	vs[10] returns the 10^th frame of the video.
	
	vs[10:20] returns frames 10-20 as a list
	
	To iterate over the full video clip you can do
	for frame in vs:
		print(frame)

	"""
	def __init__(self, file_name:str, color:bool=True):
		"""
		file_name : str
			uri of video file
		color : bool
			Pass true for VideoStream to return color images. Pass false
			for VideoStream to return b+w images. Defaults to True.
		"""
		self.video = VideoFileClip(file_name)
		self.file_name = file_name
		self.color = color
		
		self.duration = self.video.duration
		self.fps = self.video.fps
		self.nframes = floor(self.duration * self.fps)
		if self.color:
			self.shape = (*self.video.size[::-1], 3)
		else:
			self.shape = (*self.video.size[::-1], 1)

		self.frame_number = 0


	def __len__(self):
		"""
		Returns number of frames in video.
		"""
		return self.nframes


	def __getitem__(self, frame_number):
		"""
		Returns single frame if frame_number is int or a list of frames if 
		frame_number is a slice.
		"""
		if isinstance(frame_number, slice):
			indices = range(*frame_number.indices(len(self)))
			return (self._get_frame(i) for i in indices)
		else:
			return self._get_frame(frame_number)


	def __next__(self):
		if self.frame_number >= len(self):
			raise StopIteration 
		else:
			self.frame_number += 1
			return self.__getitem__(self.frame_number-1)


	def __iter__(self):
		return self


	def _get_frame(self, frame_number:int) -> np.ndarray:
		"""
		frame_number : int
			Number of the frame in the video file to be decompressed and
			returned.

		Returns
			frame : np.ndarray
				This is an array representing the image of the video file
				at that frame. The array frame will have dimensions
				(y_resolution, x_resolution, 3) if self.color is True
				or (y_resolution, x_resolution, 1) if self.color is False.
		"""

		#When frames are pulled to fast from an mp4 file using MoviePy
		#the underlying C library sometimes raises the error 
		#ALSA lib pcm.c:7963:(snd_pcm_recover) underrun occurred
		#I found this to happen when using multithreading to speed up
		#the decompression of a large number of frames from the mp4 file.
		#I'm not really sure what the error means. It seems to have something 
		#to do with reading the sound part of the mp4 file. It doesn't seem 
		#to affect the ouput of the image part of the file so I pipe stderr 
		#to devnull to supress the error. 


		#with stdchannel_redirected(sys.stderr, os.devnull):
		frame = self.video.get_frame( frame_number / self.fps )

		if self.color:
			return frame
		else:
			return color.rgb2gray(frame)

	def read(self):
		return self.__next__()

	def close(self):
		"""
		Closes and deletes connection to video file.
		"""
		self.video.__del__()


	def set_color(self, color:bool):
		"""
		color : bool
			Pass true for VideoStream to return color images. Pass false
			for VideoStream to return b+w images.
		"""
		self.color = color
		if self.color:
			self.shape = (*self.video.size[::-1], 3)
		else:
			self.shape = (*self.video.size[::-1], 1)


	def plot_frame(self, frame_number:int):
		"""
		Plots frame self[frame_number]
		frame_number : int
			Pass frame_number of frame to be plotted.

		Returns fig, ax, im of plotted frame
		"""
		fig, ax = plt.subplots()
		frame = self[frame_number]

		if self.color:
			im = ax.imshow(frame)
		else:
			im = plt.imshow(frame, cmap='Greys')

		return fig, ax, im


class ConvolutionalVideoStream(VideoStream):
	"""
	A TensorFlow convolutional layer requires that input input arrays have shape
	equal to (1, y_resolution, x_resolution, num_channels).
	This is a subclass of VideoStream that returns image arrays with this 
	dimension.
	"""
	def _get_frame(self, frame_number:int):
		frame = super()._get_frame(frame_number)
		frame.shape = (1, *self.shape)

		return frame


class FullyConnectedVideoStream(VideoStream):
	"""
	A TensorFlow fully connected layers requires that input input arrays have shape
	equal to (1, y_resolution * x_resolution * num_channels).
	This is a subclass of VideoStream that returns image arrays with this 
	dimension.
	"""
	def __init__(self, file_name:str, color:bool=False, chunk_idx:int=0, total_chunks:int=1):
		super().__init__(file_name, color)

		if chunk_idx >= total_chunks:
			raise ValueError("int chunk_idx must be less than int total_chunks")

		if self.shape[0] % total_chunks != 0 or self.shape[1] % total_chunks !=0:
			raise ValueError("The images of this video have size ({},{}). The int total_chunks must divide into both of thse numbers with 0 remainder".format(*self.shape))

		self.chunk_idx = chunk_idx
		self.total_chunks = total_chunks
		self.chunk_size = (self.shape[0] // total_chunks, self.shape[1] // total_chunks)


	def _get_frame(self, frame_number:int):
		frame = super()._get_frame(frame_number)

		if self.total_chunks == 1:
			frame.shape = (1, frame.size)
		else:
			chunked_frame = self.chunk(frame, self.total_chunks)
			frame = chunked_frame[self.chunk_idx]
			frame = frame.flatten()
			frame.shape = (1, frame.size) 

		return frame


	def chunk(self, arr, n):
		"""
		Function to split 2d array into n equal sized chunks.
		"""
		y_chunk_idxs = np.split( np.arange(0, self.shape[0]) , n )
		x_chunk_idxs = np.split( np.arange(0, self.shape[1]) , n )

		chunks = []
		for yi, xi in zip(y_chunk_idxs, x_chunk_idxs):
			chunks.append(arr[yi][:, xi])

		return chunks

def decompress_worker(training_data:Queue, frame_numbers:Queue, 
	vs:VideoStream):
	"""
	training_data : Queue
		A queue in which the decompressed images will be stored and s
		subsequently passed to the neueral network for use in training. 
	frame_numbers : Queue
		A queue which stores the frame numbers of the images in the video
		which will be decompressed.
	video_file_path : str
		The full file path of the video which will be used in training.

	Eache of these workers will pop off one integer from frame_numbers.
	It will then decompress that frame number from VideoStream vs. Finally
	it will store the decompressed image in training_data.
	"""
	if training_data.qsize() > 0:
		raise ValueError("The queue training_data should initially be empty.")

	#ffmpeg doesn't like it when different threads use the same VideoStream 
	#object to decode different frames from the same video. So here I have
	#each thread reinitialize its own VideoStream class.

	vs.__init__(vs.file_name, vs.color)

	while frame_numbers.qsize() > 0:
		#print('frame_numbers.size={}'.format(frame_numbers.qsize()))
		i = frame_numbers.get()
		frame = vs[i]
		training_data.put(frame)

	vs.close()


if __name__ == '__main__':
	video_folder = '/mnt/data_drive/frame_viewer_client'
	video_files = os.listdir(video_folder)
	video_file = video_files[2]
	full_path = os.path.join(video_folder, video_file)
	kvs = ConvolutionalVideoStream(full_path)

