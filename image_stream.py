from skimage import color
import numpy as np
import matplotlib.pyplot as plt
import os, sys
from PIL import Image
from typing import List
from math import floor
import re
from scipy.misc import imread
from scipy.io import loadmat
from sklearn.preprocessing import binarize

plt.style.use('bmh')

class ImageStream:
	"""
	Interface to provide easy iteration and indexing access to folder of images. 

	Instructions: 
	vs = VideoStream('/path/to/file.mp4')

	vs[10] returns the 10^th frame of the video.
	
	vs[10:20] returns frames 10-20 as a list
	
	To iterate over the full video clip you can do
	for frame in vs:
		print(frame)

	"""
	def __init__(self, folder_name:str, color:bool=True):
		"""
		file_name : str
			uri of video file
		color : bool
			Pass true for VideoStream to return color images. Pass false
			for VideoStream to return b+w images. Defaults to True.
		"""
		self.folder = folder_name
		self.image_files = self.get_files_in_folder(folder_name)
		regex = re.compile(r'(\d+)')
		self.image_files.sort(key = lambda s: int(regex.findall(s)[0]))
		self.color = color
		
		self.num_images = len(self.image_files)
		if self.color:
			frame = imread(self.image_files[0], mode='RGB')
		else:
			frame = imread(self.image_files[0], mode='L') / 255

		self.shape = frame.shape

		self.frame_number = 0


	def __len__(self):
		"""
		Returns number of frames in video.
		"""
		return self.num_images


	def __getitem__(self, frame_number):
		"""
		Returns single frame if frame_number is int or a list of frames if 
		frame_number is a slice.
		"""
		if isinstance(frame_number, slice):
			indices = range(*frame_number.indices(len(self)))
			return (self._get_frame(i) for i in indices)
		else:
			return self._get_frame(frame_number)


	def __next__(self):
		if self.frame_number >= len(self):
			raise StopIteration 
		else:
			self.frame_number += 1
			return self.__getitem__(self.frame_number-1)


	def __iter__(self):
		return self


	def _get_frame(self, frame_number:int) -> np.ndarray:
		"""
		frame_number : int
			Number of the frame in the video file to be decompressed and
			returned.

		Returns
			frame : np.ndarray
				This is an array representing the image of the video file
				at that frame. The array frame will have dimensions
				(y_resolution, x_resolution, 3) if self.color is True
				or (y_resolution, x_resolution, 1) if self.color is False.
		"""

		image_file = self.image_files[frame_number]

		if self.color:
			frame = imread(image_file, mode='RGB')
		else:
			frame = imread(image_file, mode='L') / 255

		return frame

	def get_files_in_folder(self, folder:str) -> List[str]:
		"""
		folder : str
			folder path
		
		Returns
			full_file_paths : List[str]
				A list of full file paths of the files in the folder.

		"""
		files = os.listdir(folder)
		full_file_paths = []
		for file in files:
			full_file_path = os.path.join(folder, file)
			full_file_paths.append(full_file_path)

		return full_file_paths

	def read(self):
		return self.__next__()

	def close(self):
		"""
		Closes and deletes connection to video file.
		"""
		self.video.__del__()


	def set_color(self, color:bool):
		"""
		color : bool
			Pass true for VideoStream to return color images. Pass false
			for VideoStream to return b+w images.
		"""
		self.color = color
		self.shape = self._get_frame(0).shape


	def plot_frame(self, frame_number:int):
		"""
		Plots frame self[frame_number]
		frame_number : int
			Pass frame_number of frame to be plotted.

		Returns fig, ax, im of plotted frame
		"""
		fig, ax = plt.subplots()
		frame = self[frame_number]

		if self.color:
			im = ax.imshow(frame)
		else:
			im = plt.imshow(frame, cmap='Greys')

		return fig, ax, im

class ChunkedImageStream(ImageStream):
	"""
	A TensorFlow fully connected layers requires that input input arrays have shape
	equal to (1, y_resolution * x_resolution * num_channels).
	This is a subclass of VideoStream that returns image arrays with this 
	dimension.
	"""
	def __init__(self, folder_name:str, color:bool=False, x_chunk_idx:int=0, 
		y_chunk_idx:int=0, total_x_chunks:int=1, total_y_chunks:int=1):
		self.x_chunk_idx = x_chunk_idx
		self.y_chunk_idx = y_chunk_idx
		self.total_x_chunks = total_x_chunks
		self.total_y_chunks = total_y_chunks

		super().__init__(folder_name, color)

		#if chunk_idx >= total_chunks:
		#	raise ValueError("int chunk_idx must be less than int total_chunks")

		#if self.shape[0] % total_chunks != 0 or self.shape[1] % total_chunks !=0:
		#	raise ValueError("The images of this video have size ({},{}). The int total_chunks must divide into both of thse numbers with 0 remainder".format(*self.shape))

		self.chunk_size = self[0].shape


	def _get_frame(self, frame_number:int):
		frame = super()._get_frame(frame_number)

		y_split = np.array_split(frame, self.total_y_chunks, axis=0)
		x_split = np.array_split(y_split[self.y_chunk_idx], self.total_x_chunks, axis=1)
		chunk_frame = x_split[self.x_chunk_idx]

		return chunk_frame


class ChunkedArrayStream:
	def __init__(self, mat_file:str, x_chunk_idx:int=0, 
		y_chunk_idx:int=0, total_x_chunks:int=1, total_y_chunks:int=1):
		self.array = loadmat(mat_file)['GT']
		self.array = np.rollaxis(self.array, 2)
		
		self.x_chunk_idx = x_chunk_idx
		self.y_chunk_idx = y_chunk_idx
		self.total_x_chunks = total_x_chunks
		self.total_y_chunks = total_y_chunks

		self.frame_number = 0
		self.shape = self.array.shape[1:]
		self.chunk_size = (self.shape[0] // total_y_chunks, self.shape[1] // total_x_chunks)

	def __len__(self):
		"""
		Returns number of frames in video.
		"""
		return len(self.array)


	def __getitem__(self, frame_number):
		"""
		Returns single frame if frame_number is int or a list of frames if 
		frame_number is a slice.
		"""
		if isinstance(frame_number, slice):
			indices = range(*frame_number.indices(len(self)))
			return (self._get_frame(i) for i in indices)
		else:
			return self._get_frame(frame_number)


	def __next__(self):
		if self.frame_number >= len(self):
			self.frame_number = 0
			raise StopIteration 
		else:
			self.frame_number += 1
			return self.__getitem__(self.frame_number-1)


	def __iter__(self):
		return self


	def _get_frame(self, frame_number:int):
		frame = self.array[frame_number]

		#binarize frame
		frame = np.where(frame > 0, 1, 0)

		y_split = np.array_split(frame, self.total_y_chunks, axis=0)
		x_split = np.array_split(y_split[self.y_chunk_idx], self.total_x_chunks, axis=1)
		chunk_frame = x_split[self.x_chunk_idx]

		return chunk_frame


def reconstruct_chunked_frame(frame_dict, image_folder, frame_number, total_x_chunks, total_y_chunks, lambda_sigma=None):

	y_chunks = []
	for y_chunk_idx in range(total_y_chunks):
		x_chunks = []
		for x_chunk_idx in range(total_x_chunks):

			chunked_ims = ChunkedImageStream(image_folder, x_chunk_idx=x_chunk_idx, 
				y_chunk_idx=y_chunk_idx, total_x_chunks=total_x_chunks, 
				total_y_chunks=total_y_chunks)
			
			key = Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)
			frame_chunk = frame_dict[key]
			frame_chunk.shape = chunked_ims.chunk_size
			x_chunks.append(frame_chunk / sum(frame_chunk))
		
		y_chunks.append(np.hstack(x_chunks))

	frame = np.vstack(y_chunks)

	return frame


if __name__ == '__main__':
	folder = './test_data/jpegs/boats'
	gt_file = './test_data/gt/boats_GT.mat'
	ims = ImageStream(folder, color=False)
	ars = ChunkedArrayStream(gt_file, chunk_idx=0, total_chunks=1)
	ims0 = ChunkedImageStream(folder, chunk_idx=0, total_chunks=2)
	ims1 = ChunkedImageStream(folder, chunk_idx=1, total_chunks=2)