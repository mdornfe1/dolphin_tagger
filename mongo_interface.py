import pymongo
import numpy as np
from bson.objectid import ObjectId
from bson.binary import Binary
import pickle


class MongoInterface:
	def __init__(self, username:str='', password:str='', 
		ip:str='', db:str='', collection:str=''):

		if len(ip) == 0:
			ip = 'localhost:27017'

		if len(username) != 0:
			uri = 'mongodb://{}:{}@{}/{}'.format(username, password, ip, db)
			self.client = pymongo.MongoClient(uri)
		else:
			uri = 'mongodb://{}/{}'.format(ip, db)
			self.client = pymongo.MongoClient(uri)

		if len(db) != 0:
			self.set_db(db)

		if len(collection) != 0:
			self.set_collection(collection)
			self.keys = self.get_keys()

		self.index = 0


	def __next__(self):
		if self.index >= len(self):
			self.index = 0
			raise StopIteration 
		else:
			self.index += 1
			return self[self.index-1]


	def __iter__(self):
		return self


	def __getitem__(self, index):
		if self.collection is None:
			raise ValueError(
				"The attribute collection must be set before method __getitem__ can be used.")
		
		if isinstance(index, slice):
			indices = index.indices(len(self))
			keys = self.keys[index]
			return [self.get_entry(key) for key in keys]
		else:
			key = self.keys[index] 
			return self.get_entry(key)


	def __len__(self):
		return len(self.keys)

	def __contains__(self, key):
		return True if key in self.keys else False


	def set_db(self, db:str):
		self.db = self.client.get_database(db)


	def set_collection(self, collection:str):
		if self.db is None:
			raise ValueError(
				"The attribute db must be set before the attribute collection can be set.")

		self.collection = self.db.get_collection(collection)
		self.keys = self.get_keys()


	def insert_entry(self, entry:dict):
		if '_id' not in entry:
			entry['_id'] = ObjectId()

		self.collection.insert_one(entry)
		self.keys = self.get_keys()


	def delete_entry(self, _id):
		self.collection.delete_one({'_id': _id })
		self.keys = self.get_keys()


	def get_entry(self, query:dict):
		entry = self.collection.find_one(query)

		return entry

	def find(self, query:dict):
		return self.collection.find(query)

	def update_entry(self, id, entry:dict):
		self.collection.update({'_id':id}, {"$set": entry}, upsert=False)


	def get_keys(self):
		return self.collection.distinct('_id')


	def close(self):
		self.client.close()



if __name__ == '__main__':
	host_ip = 'localhost'
	mi = MongoInterface(username='magnasco', password='dolphins', ip='localhost', 
		db='neural_network_metadata', collection='metadata')

"""
for entry in miv:
	_id = entry['_id']
	miv.delete_entry(_id)
"""