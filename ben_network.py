import threading
from typing import List, Generator, Tuple
from queue import Queue, deque
from tempfile import NamedTemporaryFile, mkdtemp
import os
from datetime import datetime
from collections import namedtuple

import tensorflow as tf
from tensorflow.python.framework.ops import Tensor
import keras
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, UpSampling2D, Reshape
from keras.regularizers import l2
from keras.callbacks import Callback, EarlyStopping, TensorBoard
from keras.metrics import mean_squared_error
from keras.objectives import binary_crossentropy
from keras.engine.topology import Layer
from keras import backend as K

from sklearn.metrics import precision_score, recall_score, f1_score
import numpy as np
from numpy.random import permutation
import matplotlib.pyplot as plt
from scipy.io import loadmat
from IPython import embed
from funcy import rpartial

from global_vars import QUADRO, TESLA, VIDEO_FOLDER
from secrets import mongo_username, mongo_password
from image_stream import (ImageStream, ChunkedImageStream, ChunkedArrayStream, 
	reconstruct_chunked_frame)
from mongo_interface import MongoInterface
from callbacks import LossHistory, ThresholdEarlyStopping
from utils import (list_to_queue, restart_tf_session, reduce_median, 
	create_train_generator, create_predict_generator)


def log2(x : Tensor) -> Tensor:
	return tf.log(x) / tf.log(tf.constant(2.0, dtype=x.dtype))

def binary_crossentropy(x : Tensor, x_hat : Tensor, axis : List[int]) -> Tensor:

	return tf.reduce_mean(
		x * log2(x_hat) + (1 - x) * log2(1 - x_hat) , axis = axis)


def ben_loss(
	x : Tensor, 
	x_hat : Tensor, 
	lambda_sigma : float) -> Tensor:
	"""
	Custom loss function for training background extraction networks (bens).
	"""

	#It's easier to flatten the images before finding the mean and median.

	B0 = reduce_median(tf.transpose(x_hat))
	# I divide by sigma in the next step. So I add a small float32 to F0
	# so as to prevent sigma from becoming 0 or Nan.
	
	F0 = tf.abs(x_hat - B0) + 1e-10
	
	sigma = tf.reduce_mean(tf.sqrt(F0 / lambda_sigma), axis=0)
	
	background_term = tf.reduce_mean(F0 / sigma, axis=1)
	
	regularization = lambda_sigma * tf.reduce_mean(sigma)
	
	bce = binary_crossentropy(x, x_hat, axis=1)
	
	loss = background_term + regularization

	return loss


def build_conv_ben(lambda_W, input_shape, batch_size, num_filters, optimizer, loss):
	layers = []
	nb_row = 3
	nb_col = 3
	num_pixels = np.product(input_shape)
	with tf.device(TESLA):

		#encoding layers
		
		layers.append(
			Conv2D(input_shape=input_shape, nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[0], border_mode='same', activation='relu', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

		layers.append(
			MaxPooling2D(pool_size=(2,2), border_mode='same') )

		layers.append(
			Conv2D(nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[1], border_mode='same', activation='relu', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

		layers.append(
			MaxPooling2D(pool_size=(2,2), border_mode='same') )

		#decoding layers

		layers.append(
			Conv2D(nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[2], border_mode='same', activation='relu', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

		layers.append(
			UpSampling2D(size=(2,2)) )

		
		layers.append(
			Conv2D(nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[3], border_mode='same', activation='relu', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

		layers.append(
			UpSampling2D(size=(2,2)) )
		

		layers.append(
			Conv2D(nb_row=nb_row, nb_col=nb_col, 
				nb_filter=num_filters[4], border_mode='same', activation='sigmoid', 
				W_regularizer=l2(lambda_W), init='glorot_uniform') )

	ben = Sequential()
	for layer in layers:
		ben.add(layer)

	ben.compile(optimizer, loss, metrics=[mean_squared_error])

	return ben


def train_ben(ben, train_frame_numbers, val_frame_numbers, batch_size, samples_per_epoch, 
	nb_epoch, nb_val_samples, delta_ben, patience, image_folder, x_chunk_idx, y_chunk_idx, 
	total_x_chunks, total_y_chunks):

	train_frame_numbers_queue = list_to_queue(
		train_frame_numbers, n_copies=1000)
	train_gen = create_train_generator(train_frame_numbers_queue, 
		batch_size, image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

	val_frame_numbers_queue = list_to_queue(
		val_frame_numbers, n_copies=1000)
	val_gen = create_train_generator(val_frame_numbers_queue, 
		batch_size, image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

	history = LossHistory()
	#early_stopping = EarlyStopping(monitor='val_loss', min_delta=delta_ben, patience=patience)
	early_stopping = ThresholdEarlyStopping(monitor='val_mean_squared_error', 
		threshold=delta_ben, patience=patience, mode='below', verbose=0)

	ben.fit_generator(generator=train_gen, validation_data=val_gen, 
		nb_epoch=nb_epoch, samples_per_epoch=samples_per_epoch, 
		nb_val_samples=nb_val_samples, callbacks=[history, early_stopping], 
		nb_worker=10, pickle_safe=True)

	return ben, history

def calc_metrics_on_training_set(ben, train_frame_numbers, batch_size, 
	video_file_name, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks):
		
	frame_numbers_queue = list_to_queue(train_frame_numbers, n_copies=2)
	val_samples = len(train_frame_numbers)

	train_gen = create_train_generator(frame_numbers_queue, 
		batch_size = 1, 
		video_file_name = video_file_name, 
		x_chunk_idx = x_chunk_idx, 
		y_chunk_idx = y_chunk_idx, 
		total_x_chunks = total_x_chunks, 
		total_y_chunks = total_y_chunks)

	metrics = ben.evaluate_generator(train_gen, 
		val_samples = val_samples, 
		nb_worker = 10, 
		pickle_safe = True)

	return metrics


def ben_hyper_param_search(hyper_params, num_filters, p_delta_ben, patience, optimizer, 
	loss, train_frame_numbers, val_frame_numbers, batch_size, image_folder, 
	x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks):
	"""
	Constructs a background extraction network (ben). Trains the network
	on images extracted from an image stream. Performs a random search to find
	the set of hyper parameters, which minimize the val_mean_squared_error. Returns
	the network trained on the optimal hyper parameters.
	"""
	
	vs = ChunkedImageStream(image_folder, x_chunk_idx=x_chunk_idx, 
		y_chunk_idx=y_chunk_idx, total_x_chunks=total_x_chunks, 
		total_y_chunks=total_x_chunks)
	x_train = np.array([vs[i] for i in train_frame_numbers])
	x_val = np.array([vs[i] for i in val_frame_numbers])

	input_shape = (*vs.chunk_size, 1)

	best_val_mse = np.inf
	head = True
	for hyper_param in hyper_params:
		lambda_W, = hyper_params

		ben = build_conv_ben(lambda_W, input_shape, batch_size, num_filters, optimizer, loss)
		
		if head:
			initial_weights = ben.get_weights()

			_, initial_mse = calc_metrics_on_training_set(ben, val_frame_numbers, batch_size,
			image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

			delta_ben = p_delta_ben * initial_mse

			head = False
		else:
			ben.set_weights(initial_weights)

		ben, history = train_ben(ben, train_frame_numbers, val_frame_numbers, batch_size, 
			samples_per_epoch, nb_epoch, nb_val_samples, delta_ben, patience, image_folder, 
			x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

		_, val_mse = calc_metrics_on_training_set(ben, val_frame_numbers, batch_size,
			image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

		if val_mse < best_val_mse:
			best_lambda_W = lambda_W
			best_val_mse = val_mse
			best_weights = ben.get_weights()
			best_history = history
			best_hyper_params = hyper_param

		restart_tf_session()

	best_ben = build_conv_ben(lambda_W, input_shape, batch_size, num_filters, optimizer, loss)
	best_ben.set_weights(best_weights)

	return best_ben, best_history, best_val_mse, best_hyper_params

def predict_on_training_set(ben, train_frame_numbers, batch_size, video_file_name, 
	x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks):
		
	frame_numbers_queue = list_to_queue(train_frame_numbers, n_copies=2)
	val_samples = len(train_frame_numbers)

	predict_gen = create_predict_generator(frame_numbers_queue, 
		batch_size = batch_size, 
		video_file_name = video_file_name, 
		x_chunk_idx = x_chunk_idx, 
		y_chunk_idx = y_chunk_idx, 
		total_x_chunks = total_x_chunks, 
		total_y_chunks = total_y_chunks)

	x_hat = ben.predict_generator(predict_gen, 
		val_samples = val_samples, 
		nb_worker = 10, 
		pickle_safe = True)

	return x_hat


if __name__ == '__main__':
	image_folder = './test_data/jpegs/boats'
	gt_file = './test_data/gt/boats_GT.mat'
	ground_truth = loadmat(gt_file)['GT']

	total_x_chunks = 1
	total_y_chunks = 1
	vs = ChunkedImageStream(image_folder, x_chunk_idx=0, 
		y_chunk_idx=0, total_x_chunks=total_x_chunks, total_y_chunks=total_x_chunks)
	full_vs = ImageStream(image_folder, color=False)

	
	#data parameters
	num_frames = len(vs)
	frame_numbers = permutation( np.arange(num_frames-1) )
	train_frame_numbers = [int(i) for i in frame_numbers[:25]]
	val_frame_numbers = [int(i) for i in frame_numbers[25:]]
	num_train_frames = len(train_frame_numbers)
	num_val_frames = len(val_frame_numbers)

	#hyperparameters 
	lambda_sigmas = np.arange(0.1, 1, 0.1)
	num_filters = [16, 16, 16, 16, 1]
	p_delta_ben = 0.05
	p_delta_bln = 0.1
	patience = 1
	nb_epoch = 100
	batch_size = 5
	optimizer = 'adam'
	samples_per_epoch = num_train_frames
	nb_val_samples = num_val_frames 

	num_trials = 1
	lambda_Ws = np.arange(0.001, 0.01, 0.001)
	hyper_params = [(lw,) for lw in lambda_Ws]
	hyper_param_trials = np.random.choice(
		range(len(hyper_params)), size=num_trials, replace=False)
	hyper_params = [(0,)]

	#started_training_at = datetime.now()
	#weights_dir = mkdtemp(dir='./saved_weights', prefix='weights_')

	x_hats = {}
	Bs = {}
	B_hats = {}


	x_chunk_idx = 0
	y_chunk_idx = 0	
	x_train = np.vstack([vs[i] for i in train_frame_numbers])
	x_val = np.vstack([vs[i] for i in val_frame_numbers])

	lambda_sigma = 0.9

	loss_func = rpartial(ben_loss, lambda_sigma)
	#loss_func = 'mean_squared_error'
	#loss_func = 'binary_crossentropy'

	ben, history, val_mse, hyper_params = ben_hyper_param_search(hyper_params, 
		num_filters, p_delta_ben, patience, optimizer, loss_func, train_frame_numbers, 
		val_frame_numbers, batch_size, image_folder, x_chunk_idx, y_chunk_idx, 
		total_x_chunks, total_y_chunks)

	x_hat_train = predict_on_training_set(ben, train_frame_numbers, batch_size, 
		image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

	plt.imshow(np.squeeze(x_hat_train[0]), cmap='gray')
	plt.show()
