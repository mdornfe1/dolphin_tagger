import numpy as np
from queue import Queue, deque
from keras import backend as K
import tensorflow as tf
from tensorflow import Session, Tensor
from collections import namedtuple
from typing import List, Generator, Tuple 

from image_stream import ImageStream, ChunkedImageStream

def list_to_queue(l : List, n_copies : int) -> Queue:
	"""
	l : List
		A python list to be loaded into the queue.

	n_copies : int
		Number of copies of l to load into queue

	Returns

	queue : Queue
		A threadsafe Queue that contains n_copies of List l.
	"""

	queue = Queue()
	queue.queue = deque(l * n_copies)

	return queue

def restart_tf_session() -> Session:
	"""
	Returns

	sess : Session
		New TensorFlow session.

	Restarts the TensorFlow session underlying Keras.
	"""
	K.get_session().close()
	sess = tf.Session()
	K.set_session(sess)

	return sess


def reduce_median(x : Tensor) -> Tensor:
	"""
	x : Tensor
		An n dimensional input tensor

	Returns

	median : Tensor
		An n-1 dimensional tensor. The result of reducing the tensor x 
		by calculating the median along the last dimension.

	Calculates the median of a TensorFlow tensor along the last dimension.
	"""
	size = tf.shape(x)[-1]
	
	sorted_x = tf.nn.top_k(x, k=size).values
	
	is_size_even = tf.equal( tf.mod(size, 2) , 0 )

	even_median = lambda : 0.5 * (
		sorted_x[..., size // 2] + sorted_x[..., size // 2 - 1] )
	
	odd_median = lambda : sorted_x[..., size // 2]
	
	median = tf.cond(is_size_even, even_median, odd_median)

	return median


def create_train_generator(
	frame_numbers : Queue,
	batch_size : int, 
	video_file_name : str,
	x_chunk_idx : int,
	y_chunk_idx : int,
	total_x_chunks : int,
	total_y_chunks : int,
	**kwargs) -> Generator[Tuple[np.ndarray, np.ndarray], None, None]:
		"""
		vs : ImageStream
			An object that makes a connection to the video file which will
			decompressed and used in neural network training.
		frame_numbers : List[int]
			List of frame numbers to be decompressed from ImageStream vs.
			Default is to decompress all frames of vs in order.

		Yields
			(batch, batch) : Tuple[np.ndarray, np.ndarray]
				A tuple of identical batches of images to be used for 
				training a Keras autoencoder.

		Returns a generator that can be used to train a TensorFlow network with
		images from a video file.
		"""

		vs = ChunkedImageStream(
			folder_name = video_file_name, 
			x_chunk_idx = x_chunk_idx, 
			y_chunk_idx = y_chunk_idx, 
			total_x_chunks = total_x_chunks, 
			total_y_chunks = total_x_chunks)

		while True:
			batch = []
			for _ in range(batch_size):
				i = frame_numbers.get()
				frame = vs[i].flatten()
				batch.append(frame)

			batch = np.vstack(batch)

			yield (batch, batch)


def create_predict_generator(
	frame_numbers : Queue,
	batch_size : int, 
	video_file_name : str,
	x_chunk_idx : int,
	y_chunk_idx : int,
	total_x_chunks : int,
	total_y_chunks : int,
	**kwargs) -> Generator[np.ndarray, None, None]:
		"""
		vs : ImageStream
			An object that makes a connection to the video file which will
			decompressed and used in neural network training.
		frame_numbers : List[int]
			List of frame numbers to be decompressed from ImageStream vs.
			Default is to decompress all frames of vs in order.

		Yields
			batch : np.ndarray
				A tuple of identical batches of images to be used for 
				training a Keras autoencoder.

		Returns a generator that can be used to train a TensorFlow network with
		images from a video file.
		"""

		vs = ChunkedImageStream(
			folder_name = video_file_name, 
			x_chunk_idx=x_chunk_idx, 
			y_chunk_idx=y_chunk_idx, 
			total_x_chunks=total_x_chunks, 
			total_y_chunks=total_x_chunks)

		while True:
			batch = []
			for _ in range(batch_size):
				i = frame_numbers.get()
				frame = vs[i].flatten()
				batch.append(frame)

			batch = np.vstack(batch)

			#print('Training examples left to predict = {}'.format(frame_numbers.qsize() - MAX_Q_SIZE), end='\r')
			yield batch

Key = namedtuple('Key', ['x_chunk_idx', 'y_chunk_idx', 'frame_number', 'lambda_sigma'])