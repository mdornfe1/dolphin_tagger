import matplotlib.pyplot as plt
from keras.callbacks import Callback


class LossHistory(Callback):
	def on_train_begin(self, logs={}):
		self.train_loss = []
		self.val_loss = []
		self.train_mse = []
		self.val_mse = []

	def on_epoch_end(self, epoch, logs={}):
		self.val_loss.append(float(logs.get('val_loss')))
		self.train_loss.append(float(logs.get('loss')))
		self.train_mse.append(float(logs.get('mean_squared_error')))
		self.val_mse.append(float(logs.get('val_mean_squared_error')))

	def plot_losses(self):
		fig, ax = plt.subplots(2, 1 , sharex=True)
		ax[0].plot(self.train_loss, label='train_loss')
		ax[0].plot(self.val_loss, label='val_loss')
		ax[1].plot(self.train_mse, label='train_mse')
		ax[1].plot(self.val_mse, label='val_mse')
		ax[1].set_xlabel('nb_epoch')

class ThresholdEarlyStopping(Callback):
	def __init__(self, monitor, threshold, patience, mode='below', verbose=0):
		super().__init__()

		if mode not in ['above', 'below']:
			raise ValueError("Argument mode must be either 'above' or 'below'.")

		self.monitor = monitor
		self.patience = patience
		self.verbose = verbose
		self.threshold = threshold
		self.mode = mode
		self.wait = 0
		self.stopped_epoch = 0

	def on_train_begin(self, logs={}):
		self.wait = 0       # Allow instances to be re-used

	def on_epoch_end(self, epoch, logs={}):
		current = logs.get(self.monitor)
		if current is None:
			warnings.warn('Early stopping requires %s available!' %
						  (self.monitor), RuntimeWarning)

		if self.mode == 'below':
			if current <= self.threshold:
				print(self.threshold)
				self.wait += 1
		else:
			if current > self.threshold:
				self.wait += 1

		if self.wait >= self.patience:
			self.stopped_epoch = epoch
			self.model.stop_training = True

	def on_train_end(self, logs={}):
		if self.stopped_epoch > 0 and self.verbose > 0:
			print('Epoch %05d: early stopping' % (self.stopped_epoch))