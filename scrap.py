import threading
from typing import List, Generator, Tuple
from queue import Queue, deque
from tempfile import NamedTemporaryFile, mkdtemp
import os
from datetime import datetime
from collections import namedtuple

from sklearn.metrics import precision_score, recall_score, f1_score
import numpy as np
from numpy.random import permutation
import matplotlib.pyplot as plt
from scipy.io import loadmat
from IPython import embed
from funcy import rpartial

from global_vars import QUADRO, TESLA, VIDEO_FOLDER
from secrets import mongo_username, mongo_password
from image_stream import (ImageStream, ChunkedImageStream, ChunkedArrayStream, reconstruct_chunked_frame)
from mongo_interface import MongoInterface


#from background_jpg import *

from matplotlib.widgets import Slider
import shelve

Key = namedtuple('Key', ['x_chunk_idx', 'y_chunk_idx', 'frame_number', 'lambda_sigma'])
image_folder = './test_data/jpegs/boats'
ims = ChunkedImageStream(image_folder, x_chunk_idx=0, y_chunk_idx=0, total_x_chunks=3, total_y_chunks=3, color=False)

with shelve.open('./data.db') as f:
	x_hats = f['x_hats'] 
	Bs = f['Bs']
	B_hats = f['B_hats']

lambda_sigmas = np.arange(0.1, 1, 0.1)

fig, axs = plt.subplots(1, 1)
slider_lambda_ax = plt.axes([0.25, 0.1, 0.65, 0.03])
slider_epsilon_ax = plt.axes([0.25, 0.15, 0.65, 0.03])

lambda_sigmas = np.arange(0.1, 1, 0.1)
epsilons = np.arange(0, 1, 0.001)

slider_lambda = Slider(slider_lambda_ax, 'lambda_sigma', 0, len(lambda_sigmas), valinit=lambda_sigmas[0])
slider_epsilon = Slider(slider_epsilon_ax, 'epsilon', 0, len(epsilons), valinit=epsilons[0])

def detect_foreground(x, x_hat, B_hat, epsilon):
	F = np.zeros_like(x_hat)
	mask = np.where(np.abs(x - x_hat - B_hat) > epsilon)
	F[mask] = 1

	return F


def plot_imgs(x, x_hat, B, B_hat, F):
	axs[0].imshow(B, cmap='gray')
	#axs[1].imshow(B_hat, cmap='gray')
	#axs[2].imshow(B, cmap='gray')
	#axs[3].imshow(B_hat, cmap='gray')
	#axs[4].imshow(F, cmap='gray')


	return fig, axs

def update(i):
	lambda_index = int(slider_lambda.val)
	epsilon_index = int(slider_epsilon.val)
	frame_number = 1
	lambda_sigma = lambda_sigmas[lambda_index]
	epsilon = epsilons[epsilon_index]
	key =  Key(None, None, frame_number, lambda_sigma)
	x = ims[frame_number]
	x_hat = x_hats[key]
	B = Bs[key]
	B_hat = B_hats[key]
	B_hat = B_hat / np.amax(B_hat)
	x = x / np.amax(x)
	F = detect_foreground(x, x_hat, B_hat, epsilon)

	axs[0].cla()
	#axs[1].cla()
	#axs[2].cla()
	#axs[3].cla()
	#axs[4].cla()

	plot_imgs(x, x_hat, B, B_hat, F)
	fig.canvas.draw_idle()

slider_lambda.on_changed(update)
slider_epsilon.on_changed(update)
plt.show()


fig, ax = plt.subplots(total_y_chunks, total_x_chunks):

for x_chunk_idx in range(total_x_chunks):
	for y_chunk_idx in range(total_y_chunks):
	
	ims = ChunkedImageStream(image_folder,
		x_chunk_idx=x_chunk_idx, 
		y_chunk_idx=y_chunk_idx,
		total_x_chunks=total_x_chunks, 
		total_y_chunks=total_y_chunks, 
		color=False)

	im = ims[0]

	ax[x_chunk_idx, y_chunk_idx].imshow(im, cmap='gray')

