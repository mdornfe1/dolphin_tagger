import shelve
from utils import Key
from image_stream import ChunkedImageStream, ChunkedArrayStream
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np
from sklearn.metrics import precision_score, recall_score, f1_score, fbeta_score

def reconstruct_chunked_frame(frame_dict, frame_number, total_x_chunks, total_y_chunks):

	y_chunks = []
	for y_chunk_idx in range(total_y_chunks):
		x_chunks = []
		for x_chunk_idx in range(total_x_chunks):

			key = Key(x_chunk_idx, y_chunk_idx, frame_number, None)
			frame_chunk = frame_dict[key]
			x_chunks.append(frame_chunk)
		
		y_chunks.append(np.hstack(x_chunks))

	try: 
		frame = np.vstack(y_chunks)
	except:
		y_chunks[0] = y_chunks[0][:,0:-1]		
		frame = np.vstack(y_chunks)

	return frame

def detect_foreground(x, x_hat, B_hat, epsilon):
	F = np.zeros_like(x)
	mask = np.where(np.abs(x - x_hat - B_hat) > epsilon)
	F[mask] = 1

	return F


with shelve.open('./data.db') as f:
	x_hats = f['x_hats'] 
	Bs = f['Bs']
	B_hats = f['B_hats']

image_folder = './test_data/jpegs/boats'
gt_file = './test_data/gt/boats_GT.mat'

total_x_chunks = 3
total_y_chunks = 3

x_chunk_idx = 0
y_chunk_idx = 0

ims = ChunkedImageStream(
	folder_name = image_folder, 
	x_chunk_idx=x_chunk_idx, 
	y_chunk_idx=y_chunk_idx, 
	total_x_chunks=total_x_chunks, 
	total_y_chunks=total_y_chunks)

ars = ChunkedArrayStream(
	mat_file = gt_file,
	x_chunk_idx = x_chunk_idx, 
	y_chunk_idx = y_chunk_idx, 
	total_x_chunks = total_x_chunks, 
	total_y_chunks = total_y_chunks)


lambda_sigmas = np.arange(0.1, 1, 0.1)
epsilons = np.arange(0, 1, 0.01)
frame_numbers = range(len(ims)-1)


#best_epsilons = []
#best_lambda_sigmas = []
F_preds = {}
best_epsilons = {}
best_lambda_sigmas = {}
best_score = 0
for x_chunk_idx in range(total_x_chunks):
	for y_chunk_idx in range(total_y_chunks):
		
		ims = ChunkedImageStream(
			folder_name = image_folder, 
			x_chunk_idx = x_chunk_idx, 
			y_chunk_idx = y_chunk_idx, 
			total_x_chunks=total_x_chunks, 
			total_y_chunks=total_y_chunks)

		ars = ChunkedArrayStream(
			mat_file = gt_file,
			x_chunk_idx = x_chunk_idx, 
			y_chunk_idx = y_chunk_idx, 
			total_x_chunks = total_x_chunks, 
			total_y_chunks = total_y_chunks)
		
		best_score = 0
		for lambda_sigma in lambda_sigmas:
			for epsilon in epsilons:
				F_pred_list = []			
				F_true_list = []
				for frame_number in frame_numbers:
					x = ims[frame_number]
					x_hat = x_hats[Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)]
					B_hat = B_hats[Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)]
					F_pred = detect_foreground(x, x_hat, B_hat, epsilon)
					F_pred_list.append(F_pred.flatten())
					
					
					F_true_list.append(ars[frame_number].flatten())
					#score = fbeta_score(F_true.flatten(), F_pred.flatten(), beta=10)
				
				score = fbeta_score(np.hstack(F_true_list), np.hstack(F_pred_list), 0.8)

				if score > best_score:
					best_score = score
					best_epsilon = epsilon
					best_lambda_sigma = lambda_sigma
					best_F_pred = F_pred

		if best_score == 0:
			best_F_pred = np.zeros_like(F_pred)

			#best_lambda_sigmas.append(best_lambda_sigma)
			#best_epsilons.append(best_epsilon)
		epsilon = best_epsilon
		lambda_sigma = best_lambda_sigma
		for frame_number in frame_numbers:
			x = ims[frame_number]
			x_hat = x_hats[Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)]
			B_hat = B_hats[Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)]
			F_pred = detect_foreground(x, x_hat, B_hat, epsilon)
			F_preds[Key(x_chunk_idx, y_chunk_idx, frame_number, None)] = F_pred
		
		best_epsilons[Key(x_chunk_idx, y_chunk_idx, None, None)] = best_epsilon
		best_lambda_sigmas[Key(x_chunk_idx, y_chunk_idx, None, None)] = best_lambda_sigma


for frame_number in frame_numbers:
	frame = reconstruct_chunked_frame(F_preds, frame_number, total_x_chunks, total_y_chunks)
	F_preds[Key(None, None, frame_number, None)] = frame


with shelve.open('./data.db') as f:
	f['F_preds'] = F_preds
	f['best_epsilons'] = best_epsilons
	f['best_lambda_sigmas'] = best_lambda_sigmas

from matplotlib.animation import FuncAnimation

fig, ax = plt.subplots()
ax.axes.get_xaxis().set_visible(False)
ax.axes.get_yaxis().set_visible(False)
def animate(frame_number):
	ax.cla()
	ax.imshow(F_preds[Key(None,None,frame_number,None)], cmap='gray') 

ani = FuncAnimation(fig, animate, frames=range(len(ims) - 1))

Writer = animation.writers['ffmpeg']
writer = Writer(fps=3, bitrate=3000)
ani.save('boat_bw.mp4', writer=writer)

ims = ChunkedImageStream(folder_name = image_folder, color=True)

fig, ax = plt.subplots()
ax.axes.get_xaxis().set_visible(False)
ax.axes.get_yaxis().set_visible(False)
def animate(frame_number):
	ax.cla()
	ax.imshow(ims[frame_number]) 

ani = FuncAnimation(fig, animate, frames=range(len(ims) - 1))

Writer = animation.writers['ffmpeg']
writer = Writer(fps=3, bitrate=3000)
ani.save('boat.mp4', writer=writer)
