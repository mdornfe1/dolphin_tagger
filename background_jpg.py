import threading
from typing import List, Generator, Tuple
from queue import Queue, deque
from tempfile import NamedTemporaryFile, mkdtemp
import os
from datetime import datetime
from collections import namedtuple

import tensorflow as tf
from tensorflow.python.framework.ops import Tensor
import keras
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout
from keras.regularizers import l2
from keras.callbacks import Callback, EarlyStopping, TensorBoard
from keras.metrics import mean_squared_error
from keras.objectives import binary_crossentropy
from keras.engine.topology import Layer
from keras import backend as K

from sklearn.metrics import precision_score, recall_score, f1_score
import numpy as np
from numpy.random import permutation
import matplotlib.pyplot as plt
from scipy.io import loadmat
from IPython import embed
from funcy import rpartial

from global_vars import QUADRO, TESLA, VIDEO_FOLDER
from secrets import mongo_username, mongo_password
from image_stream import (ImageStream, ChunkedImageStream, ChunkedArrayStream, 
	reconstruct_chunked_frame)
from mongo_interface import MongoInterface
from callbacks import LossHistory, ThresholdEarlyStopping
from utils import (list_to_queue, restart_tf_session, reduce_median, 
	create_train_generator, create_predict_generator)


def calc_static_background(x_hat):

	B0 = np.median(x_hat, axis=0)
	B0.shape = (1, len(B0))

	return B0


def calc_dynamic_backgrounds(x_hat, B0, lambda_sigma):
	sigma = np.mean(np.sqrt(np.abs((x_hat - B0) / lambda_sigma)), axis=0)
	true_idxs = np.where( np.abs((x_hat-B0)) <= sigma )
	B = np.repeat(B0, repeats=len(x_hat), axis=0)
	B[true_idxs] = x_hat[true_idxs]

	return B


def detect_foreground(x_hat, B_hat, epsilon):
	F = np.zeros_like(x_hat)
	mask = np.where(np.abs(x_hat - B_hat) > epsilon)
	F[mask] = 1

	return F


def ben_loss(
	x : Tensor, 
	x_hat : Tensor, 
	lambda_sigma : float) -> Tensor:
	"""
	Custom loss function for training background extraction networks (bens).
	"""
	B0 = reduce_median(tf.transpose(x_hat))
	# I divide by sigma in the next step. So I add a small float32 to F0
	# so as to prevent sigma from becoming 0 or Nan.
	F0 = tf.abs(x_hat - B0) + 1e-10
	sigma = tf.reduce_mean(tf.sqrt(F0 / lambda_sigma), axis=0)
	background_term = tf.reduce_mean(F0 / sigma, axis=-1)
	regularization = lambda_sigma * tf.reduce_mean(sigma)
	bce = binary_crossentropy(x, x_hat)
	loss = bce + background_term + regularization

	return loss


def build_ben(lambda_W, dropout_rate, num_neurons, optimizer, loss, **kwargs):
	layers = []
	with tf.device(TESLA):
		layers.append(
			Dense(input_dim=num_neurons[0], output_dim=num_neurons[1], activation='relu', W_regularizer=l2(lambda_W), init='glorot_uniform'))
		layers.append(Dropout(dropout_rate))
		layers.append(
			Dense(input_dim=num_neurons[1], output_dim=num_neurons[2], activation='relu', W_regularizer=l2(lambda_W), init='glorot_uniform'))
		layers.append(Dropout(dropout_rate))
	
	with tf.device(QUADRO):
		layers.append(Dense(input_dim=num_neurons[2], output_dim=num_neurons[3], activation='relu', W_regularizer=l2(lambda_W), init='glorot_uniform'))
		layers.append(Dropout(dropout_rate))
		layers.append(Dense(input_dim=num_neurons[3], output_dim=num_neurons[4], activation='sigmoid', W_regularizer=l2(lambda_W), init='glorot_uniform'))

	ben = Sequential()
	for layer in layers:
		ben.add(layer)

	ben.compile(optimizer, loss, metrics=[mean_squared_error])

	return ben


def predict_on_training_set(ben, train_frame_numbers, batch_size, video_file_name, 
	x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks):
		
	frame_numbers_queue = list_to_queue(train_frame_numbers, n_copies=2)
	val_samples = len(train_frame_numbers)

	predict_gen = create_predict_generator(frame_numbers_queue, 
		batch_size = batch_size, 
		video_file_name = video_file_name, 
		x_chunk_idx = x_chunk_idx, 
		y_chunk_idx = y_chunk_idx, 
		total_x_chunks = total_x_chunks, 
		total_y_chunks = total_y_chunks)

	x_hat = ben.predict_generator(predict_gen, 
		val_samples = val_samples, 
		nb_worker = 10, 
		pickle_safe = True)

	return x_hat


def calc_metrics_on_training_set(ben, train_frame_numbers, batch_size, 
	video_file_name, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks):
		
	frame_numbers_queue = list_to_queue(train_frame_numbers, n_copies=2)
	val_samples = len(train_frame_numbers)

	train_gen = create_train_generator(frame_numbers_queue, 
		batch_size = 1, 
		video_file_name = video_file_name, 
		x_chunk_idx = x_chunk_idx, 
		y_chunk_idx = y_chunk_idx, 
		total_x_chunks = total_x_chunks, 
		total_y_chunks = total_y_chunks)

	metrics = ben.evaluate_generator(train_gen, 
		val_samples = val_samples, 
		nb_worker = 10, 
		pickle_safe = True)

	return metrics


def train_ben(ben, train_frame_numbers, val_frame_numbers, batch_size, samples_per_epoch, 
	nb_epoch, nb_val_samples, delta_ben, patience, image_folder, x_chunk_idx, y_chunk_idx, 
	total_x_chunks, total_y_chunks):

	train_frame_numbers_queue = list_to_queue(
		train_frame_numbers, n_copies=1000)
	train_gen = create_train_generator(train_frame_numbers_queue, 
		batch_size, image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

	val_frame_numbers_queue = list_to_queue(
		val_frame_numbers, n_copies=1000)
	val_gen = create_train_generator(val_frame_numbers_queue, 
		batch_size, image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

	history = LossHistory()
	#early_stopping = EarlyStopping(monitor='val_loss', min_delta=delta_ben, patience=patience)
	early_stopping = ThresholdEarlyStopping(monitor='val_mean_squared_error', 
		threshold=delta_ben, patience=patience, mode='below', verbose=0)

	ben.fit_generator(generator=train_gen, validation_data=val_gen, 
		nb_epoch=nb_epoch, samples_per_epoch=samples_per_epoch, 
		nb_val_samples=nb_val_samples, callbacks=[history, early_stopping], 
		nb_worker=10, pickle_safe=True)

	return ben, history


def train_bln(bln, B_train, B_val, batch_size, nb_epoch, delta_bln, patience):
	history = LossHistory()
	#early_stopping = EarlyStopping(monitor='val_loss', min_delta=delta_bln, 
	#	patience=patience)
	early_stopping = ThresholdEarlyStopping(monitor='val_mean_squared_error', 
		threshold=delta_bln, patience=patience, mode='below', verbose=0)
		
	bln.fit(B_train, B_train, validation_data=(B_val, B_val), 
		batch_size=batch_size, nb_epoch=nb_epoch, callbacks=[history, early_stopping])

	return bln, history

def calc_optimal_ben(lambda_Ws, dropout_rates, p_delta_ben, patience, optimizer, 
	loss, train_frame_numbers, val_frame_numbers, batch_size, image_folder, 
	x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks):
	"""
	Constructs a background extraction network (ben). Trains the network
	on images extracted from an image stream. Performs a random search to find
	the set of hyper parameters, which minimize the val_mean_squared_error. Returns
	the network trained on the optimal hyper parameters.
	"""
	
	vs = ChunkedImageStream(image_folder, x_chunk_idx=x_chunk_idx, 
		y_chunk_idx=y_chunk_idx, total_x_chunks=total_x_chunks, 
		total_y_chunks=total_x_chunks)
	#x_train = np.vstack([vs[i].flatten() for i in train_frame_numbers])
	#x_val = np.vstack([vs[i].flatten() for i in val_frame_numbers])

	layer0_size = vs.chunk_size[0] * vs.chunk_size[1]
	layer1_size = layer0_size // 2
	layer2_size = layer1_size // 2
	num_neurons = [layer0_size, layer1_size, layer2_size, layer1_size, layer0_size]

	num_trials = 2
	hyper_params = [(lw, dr) for dr in dropout_rates for lw in lambda_Ws]
	hyper_param_trials = np.random.choice(
		range(len(hyper_params)), size=num_trials, replace=False)

	best_val_mse = np.inf

	head = True
	for trial in hyper_param_trials[1:]:
		lambda_W, dropout_rate = hyper_params[trial]

		ben = build_ben(lambda_W, dropout_rate, num_neurons, optimizer, loss)
		
		if head:
			initial_weights = ben.get_weights()

			_, initial_mse = calc_metrics_on_training_set(ben, val_frame_numbers, batch_size,
			image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

			delta_ben = p_delta_ben * initial_mse

			head = False
		else:
			ben.set_weights(initial_weights)

		ben, history = train_ben(ben, train_frame_numbers, val_frame_numbers, batch_size, 
			samples_per_epoch, nb_epoch, nb_val_samples, delta_ben, patience, image_folder, 
			x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

		_, val_mse = calc_metrics_on_training_set(ben, val_frame_numbers, batch_size,
			image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

		if val_mse < best_val_mse:
			best_lambda_W = lambda_W
			best_dropout_rate = dropout_rate
			best_val_mse = val_mse
			best_weights = ben.get_weights()
			best_history = history
			best_hyper_params = hyper_params[trial]

		restart_tf_session()

	best_ben = build_ben(best_lambda_W, best_dropout_rate, num_neurons, optimizer, loss)
	best_ben.set_weights(best_weights)

	return best_ben, best_history, best_val_mse, best_hyper_params


if __name__ == '__main__':
	image_folder = './test_data/jpegs/boats'
	gt_file = './test_data/gt/boats_GT.mat'
	ground_truth = loadmat(gt_file)['GT']

	total_x_chunks = 3
	total_y_chunks = 3
	vs = ChunkedImageStream(image_folder, x_chunk_idx=0, 
		y_chunk_idx=0, total_x_chunks=total_x_chunks, total_y_chunks=total_x_chunks)
	full_vs = ImageStream(image_folder, color=False)

	
	#data parameters
	num_frames = len(vs)
	#num_train_frames = 8 * num_frames // 10
	#num_val_frames = num_frames - num_train_frames
	frame_numbers = permutation( np.arange(num_frames-1) )
	#train_frame_numbers = [int(i) for i in frame_numbers[:num_train_frames]]
	#val_frame_numbers = [int(i) for i in frame_numbers[num_train_frames:]]
	train_frame_numbers = [int(i) for i in frame_numbers[:25]]
	val_frame_numbers = [int(i) for i in frame_numbers[25:]]
	num_train_frames = len(train_frame_numbers)
	num_val_frames = len(val_frame_numbers)

	#hyperparameters 
	layer0_size = vs.chunk_size[0] * vs.chunk_size[1]
	layer1_size = layer0_size // 2
	layer2_size = layer1_size // 2
	num_neurons = [layer0_size, layer1_size, layer2_size, layer1_size, layer0_size]
	p_delta_ben = 0.05
	p_delta_bln = 0.1
	patience = 1
	nb_epoch = 20
	batch_size = 5
	nb_worker = 10
	optimizer = 'adam'
	samples_per_epoch = num_train_frames
	nb_val_samples = num_val_frames 

	#started_training_at = datetime.now()
	#weights_dir = mkdtemp(dir='./saved_weights', prefix='weights_')

	Key = namedtuple('Key', ['x_chunk_idx', 'y_chunk_idx', 'frame_number', 'lambda_sigma'])
	x_hats = {}
	Bs = {}
	B_hats = {}
	dropout_rates = np.arange(0, 1 , 0.1)
	lambda_Ws = np.array([10**n for n in range(-10, 1)])
	lambda_sigmas = np.arange(0.1, 1, 0.1)

	x_chunk_idx = 0
	y_chunk_idx = 0	
	lambda_sigma = lambda_sigmas[5]


	x_train = np.vstack([vs[i].flatten() for i in train_frame_numbers])
	x_val = np.vstack([vs[i].flatten() for i in val_frame_numbers])
	

	for y_chunk_idx in range(total_y_chunks):
		for x_chunk_idx in range(total_x_chunks):
			for lambda_sigma in lambda_sigmas:
				
				loss_func = rpartial(ben_loss, lambda_sigma)

				ben, history, val_mse, hyper_params = calc_optimal_ben(
					lambda_Ws, dropout_rates, p_delta_ben, patience, optimizer, 
					loss_func, train_frame_numbers, val_frame_numbers, batch_size, image_folder, 
					x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)

				x_hat_train = predict_on_training_set(ben, train_frame_numbers, batch_size, 
					image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)
				x_hat_val = predict_on_training_set(ben, val_frame_numbers, batch_size,
					image_folder, x_chunk_idx, y_chunk_idx, total_x_chunks, total_y_chunks)
				
				B0_trains = []
				for batch in np.array_split(x_hat_train, batch_size):
					B0_trains.append(calc_static_background(batch))

				B0_vals = []
				for batch in np.array_split(x_hat_val, batch_size):
					B0_vals.append(calc_static_background(batch))

				for frame_number, xht, in zip(train_frame_numbers, x_hat_train):
					key = Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)
					x_hats[key] = xht

				for frame_number, xhv, in zip(val_frame_numbers, x_hat_val):
					key = Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)
					x_hats[key] = xhv

				embed()

				bln = ben
				bln.compile(optimizer, binary_crossentropy, metrics=[mean_squared_error])
				#ben_checkpoint = NamedTemporaryFile(dir=weights_dir, prefix='ben_', suffix='.h5', delete=True)
				#ben.save(ben_checkpoint.name)

				B_train = []
				for batch, B0_train in zip(np.array_split(x_hat_train, batch_size), B0_trains):
					B_train.append(calc_dynamic_backgrounds(batch, B0_train, lambda_sigma))

				B_train = np.vstack(B_train)
				
				B_val = []
				for batch, B0_val in zip(np.array_split(x_hat_val, batch_size), B0_vals):
					B_val.append(calc_dynamic_backgrounds(batch, B0_val, lambda_sigma))

				B_val = np.vstack(B_val)

				_, initial_mse = bln.evaluate(B_val, B_val, batch_size=batch_size, verbose=1)
				_, train_mse = bln.evaluate(B_train, B_train, batch_size=batch_size, verbose=1)
				delta_bln = p_delta_bln * initial_mse

				print('training bln for x_chunk_idx={}, y_chunk_idx={}, lambda_sigma={}'.format(x_chunk_idx, y_chunk_idx, lambda_sigma))
				bln, history_bln = train_bln(bln, B_train, B_val, batch_size, nb_epoch, delta_bln, patience)

				B_hat_train = bln.predict(B_train)
				B_hat_val = bln.predict(B_val)


				for frame_number, B, B_hat in zip(train_frame_numbers, B_train, B_hat_train):
					key = Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)
					Bs[key] = B
					B_hats[key] = B_hat

				for frame_number, B, B_hat in zip(val_frame_numbers, B_val, B_hat_val):
					key = Key(x_chunk_idx, y_chunk_idx, frame_number, lambda_sigma)
					Bs[key] = B
					B_hats[key] = B_hat

				restart_tf_session()


	for frame_number in train_frame_numbers + val_frame_numbers:
		for lambda_sigma in lambda_sigmas:
			key = Key(None, None, frame_number, lambda_sigma)
			x_hats[key] = reconstruct_chunked_frame(x_hats, image_folder, frame_number, total_x_chunks, total_y_chunks, lambda_sigma)
			Bs[key] = reconstruct_chunked_frame(Bs, image_folder, frame_number, total_x_chunks, total_y_chunks, lambda_sigma)
			B_hats[key] = reconstruct_chunked_frame(B_hats, image_folder, frame_number, total_x_chunks, total_y_chunks, lambda_sigma)

	"""
	with shelve.open('./data.db') as f:
		f['x_hats'] = x_hats
		f['Bs'] = Bs 
		f['B_hats'] = B_hats
	"""


	frame_number = val_frame_numbers[3]
	d = np.abs(full_vs[frame_number] - B_hats[Key(None,None, frame_number,1)])    

	epsilons = np.arange(0, 1, 0.01)
	epsilon = 0.5
	for lambda_sigma in lambda_sigmas:
		for frame_number in val_frame_numbers:
			x_hat = x_hats[Key(None, None, frame_number, None)]
			B_hat = B_hats[Key(None, None, frame_number, lambda_sigma)]
			F = detect_foreground(x_hat, B_hat, epsilon)

"""
ground_truths = ChunkedArrayStream(gt_file, chunk_idx=chunk_idx, total_chunks=total_chunks)
			val_ground_truths = np.vstack([ground_truths[frame_number] for frame_number in val_frame_numbers])
	for epsilon in epsilons:
		F_vals[(chunk_idx, lambda_sigma, epsilon)] = []
		precisions[(chunk_idx, lambda_sigma, epsilon)] = []
		recalls[(chunk_idx, lambda_sigma, epsilon)] = []
		f1_scores[(chunk_idx, lambda_sigma, epsilon)] = []

		for x_hat, B_hat, val_ground_truth in zip(x_hat_val, B_hat_val, val_ground_truths):
			F_val = detect_foreground(x_hat, B_hat, epsilon=epsilon)
			precision = precision_score(val_ground_truth, F_val) 
			recall = recall_score(val_ground_truth, F_val)
			f1 = f1_score(val_ground_truth, F_val)
			
			F_vals[(chunk_idx, lambda_sigma, epsilon)].append(F_val)
			precisions[(chunk_idx, lambda_sigma, epsilon)].append(precision)
			recalls[(chunk_idx, lambda_sigma, epsilon)].append(recall)
			f1_scores[(chunk_idx, lambda_sigma, epsilon)].append(f1)

"""			